public class InfiniteTimeLoop {
    public static void main(String args[]){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("You're stuck in a time loop");
            System.out.println("Sinong pinakaPogi sa Backend?");
            String input = sc.nextLine();
            if (input.equals("Master Philip")) {
                System.out.println("Facts!");
                break;
            }
        }
    }
}
